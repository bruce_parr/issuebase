/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package burp;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.InputEvent;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 *
 * @author BParr
 */
public class BurpExtender implements IBurpExtender, IHttpListener, IExtensionStateListener, IContextMenuFactory, IContextMenuInvocation, ITab {

    private IBurpExtenderCallbacks callbacks;
    private PrintWriter stdout;  
    private PrintWriter stderr;  
    private String MAIN_TAB_NAME = "IssueBase";
    private JPanel panel;

    @Override
    public void registerExtenderCallbacks (IBurpExtenderCallbacks callbacks)
    {
        this.callbacks = callbacks;
        
        this.callbacks.setExtensionName ("IssueBase");
        
        stdout = new PrintWriter(callbacks.getStdout(), true);
        stderr = new PrintWriter(callbacks.getStderr(), true);

        this.callbacks.registerExtensionStateListener(this);
        this.callbacks.registerContextMenuFactory(this);
        
        MainPanel.callbacks = callbacks;
        MainPanel.mainPanelGui = new MainPanelGui();
        this.callbacks.addSuiteTab(this);
        this.createMenuItems(this);
        
        stdout.write("Sqlite Connecting");
        SqliteDb db = new SqliteDb();
        db.Connect();
        stdout.write("Sqlite Connected");
    }

    @Override
    public String getTabCaption()
    {
        return MainPanel.extensionName;
    }

    @Override
    public Component getUiComponent()
    {
        return MainPanel.mainPanelGui;
    }

    @Override
    public void processHttpMessage(int toolFlag, boolean messageIsRequest, IHttpRequestResponse messageInfo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void extensionUnloaded() {
        stdout.println("Extension unloading");
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        stdout.println("Extension unloaded");
    }

    @Override
    public List<JMenuItem> createMenuItems(IContextMenuInvocation invocation) {
        List<JMenuItem> menuItems  = new ArrayList<>();
        JMenuItem menuItem = new JMenuItem("Add to IssueBase");
        menuItems.add(menuItem);
        stdout.write("menuItems.size" + menuItems.size());
        return menuItems;
    }

    @Override
    public InputEvent getInputEvent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getToolFlag() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public byte getInvocationContext() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int[] getSelectionBounds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IHttpRequestResponse[] getSelectedMessages() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public IScanIssue[] getSelectedIssues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
